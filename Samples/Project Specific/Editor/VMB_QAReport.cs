﻿using KobGamesSDKSlim.ProjectValidator;
using KobGamesSDKSlim.ProjectValidator.Modules;
using KobGamesSDKSlim.ProjectValidator.Modules.Build;
using UnityEditor;
using UnityEngine;

namespace KobGamesSDKSlim.QAReport
{
    //TODO - this should be outside of Samples and in an Editor Assembly. We would need to do lots of changes to make that work though so we won't for now
    [ValidatorModuleOrder(100)]
    public class VMB_QAReport : ValidatorModuleBuild
    {
        public override eBuildValidatorResult Validate()
        {
            var qaReportObject = Object.FindObjectOfType<QAReport>();
            if (qaReportObject != null)
            {
                qaReportObject.AddValidationLogs(ValidatorBuild.WarningsList, ValidatorBuild.ErrorList);
                PrefabUtility.ApplyPrefabInstance(qaReportObject.gameObject, InteractionMode.AutomatedAction);
            }

            return eBuildValidatorResult.AllGood;
        }
    }
}