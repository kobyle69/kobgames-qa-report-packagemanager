using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using UnityEngine.Profiling;
using Unity.Profiling;
using System.IO;
using KobGamesSDKSlim.GameManagerV1;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace KobGamesSDKSlim.QAReport
{
	public class QAReport : Singleton<QAReport>
	{
		
	#region Vars

		//Template code in case we want to profile some specific thing
		//public static ProfilerMarker s_ExampleMarker = new ProfilerMarker("ExampleMarker.Example");

	#region Inspector

		[SerializeField]           private float     m_ReportFrequency = 1;
		[SerializeField]           private Button    m_SaveReportButton;
		[SerializeField]           private Transform m_PlayerTransform;
		[SerializeField, ReadOnly] private string    m_QAReportVersion;
		[SerializeField, ReadOnly] private bool      m_RecordingLevel;

		[SerializeField, ReadOnly] private List<string> m_ValidationWarnings = new List<string>(),
														m_ValidateErrors     = new List<string>();

		[ShowInInspector, ReadOnly] private QAReportData m_Data          = new QAReportData();
		[ShowInInspector, ReadOnly] private QA_LevelInfo m_TempLevelInfo = new QA_LevelInfo();

	#endregion

	#region Private

		private float m_LastTime;
		private float m_TimeToAddEntry;

		private ProfilerRecorder m_MainThreadTimeRecorder,
								 m_DrawcallsRecorder,
								 m_SetPassCallsRecorder,
								 m_TrianglesRecorder,
								 m_SystemMemoryRecorder,
								 m_GCMemoryRecorder;

	#endregion

	#region Properties

		public Transform PlayerTransform
		{
			set => m_PlayerTransform = value;
		}

		private string FileName => m_Data.BuildInfo.Name + "_" + m_Data.BuildInfo.Version + "_" + DateTime.Now.ToString("y-MM-dd_HH-mm") + ".json";

		private int   Profiler_FPS          => (int) Math.Round(1000 / (getRecorderFrameAverage(m_MainThreadTimeRecorder) * (1e-6f)));
		private int   Profiler_Batches      => (int) m_DrawcallsRecorder.LastValue;
		private int   Profiler_SetPassCalls => (int) m_SetPassCallsRecorder.LastValue;
		private int   Profiler_Triangles    => (int) m_TrianglesRecorder.LastValue;
		private float Profiler_SystemMemory => (float) Math.Round((m_SystemMemoryRecorder.LastValue / (1024f * 1024f)), 2);
		private float Profiler_GCMemory     => (float) Math.Round((m_GCMemoryRecorder.LastValue     / (1024f * 1024f)), 2);

		//Used since Profiler doesn't give values on StartUp
		private float SystemMemory => (float) Math.Round(Profiler.GetTotalAllocatedMemoryLong() / (1024f * 1024f), 2);
		private float GCMemory     => (float) Math.Round(Profiler.GetMonoUsedSizeLong()         / (1024f * 1024f), 2);


		private string m_TimeStamp => (Time.timeSinceLevelLoad - m_LastTime).ToString(QAReportStaticValues.s_CultureInfo);
		private string m_PlayerPos => m_PlayerTransform != null ? m_PlayerTransform.position.ToString() : "";

	#endregion

	#endregion
		
		
	#region Public Interface

		public void AddValidationLogs(List<string> i_Warnings, List<string> i_Errors)
		{
			m_ValidationWarnings = new List<string>(i_Warnings);
			m_ValidateErrors     = new List<string>(i_Errors);
		}

	#endregion

		

	#region Unity Init Calls

		#if UNITY_EDITOR
		private void OnValidate()
		{
			m_QAReportVersion = KobGamesSDKSlim.QAReport.UI.QAReportWindow_Editor.GetReadmeVersion();
		}
#endif
		
		private void OnEnable()
		{
			if (!Debug.isDebugBuild)
			{
				gameObject.SetActive(false);
				return;
			}

			GameManagerBase.OnLevelStarted          += OnLevelStarted;
			GameManagerBase.OnLevelCompleted        += OnLevelFinished;
			GameManagerBase.OnLevelFailed           += OnLevelFinished;
			GameManagerBase.OnLevelFailedNoContinue += OnLevelFinished;
			GameManagerBase.OnLevelReset            += OnLevelFinished;
			GameManagerBase.OnGameReset             += OnLevelFinished;

			Application.logMessageReceived += logMessagesCallback;

			m_MainThreadTimeRecorder = ProfilerRecorder.StartNew(ProfilerCategory.Internal, "Main Thread", 15);
			m_DrawcallsRecorder      = ProfilerRecorder.StartNew(ProfilerCategory.Render,   "Draw Calls Count");
			m_SetPassCallsRecorder   = ProfilerRecorder.StartNew(ProfilerCategory.Render,   "SetPass Calls Count");
			m_TrianglesRecorder      = ProfilerRecorder.StartNew(ProfilerCategory.Render,   "Triangles Count");
			m_SystemMemoryRecorder   = ProfilerRecorder.StartNew(ProfilerCategory.Memory,   "Total Used Memory");
			m_GCMemoryRecorder       = ProfilerRecorder.StartNew(ProfilerCategory.Memory,   "GC Reserved Memory");

			m_SaveReportButton.onClick.RemoveAllListeners();
			m_SaveReportButton.onClick.AddListener(exportData);
			m_SaveReportButton.gameObject.SetActive(false);

			InitialSetup();
		}

		public override void OnDisable()
		{
			base.OnDisable();

			if (!Debug.isDebugBuild)
				return;

			GameManagerBase.OnLevelStarted          -= OnLevelStarted;
			GameManagerBase.OnLevelCompleted        -= OnLevelFinished;
			GameManagerBase.OnLevelFailed           -= OnLevelFinished;
			GameManagerBase.OnLevelFailedNoContinue -= OnLevelFinished;
			GameManagerBase.OnLevelReset            -= OnLevelFinished;
			GameManagerBase.OnGameReset             -= OnLevelFinished;

			Application.logMessageReceived -= logMessagesCallback;

			m_MainThreadTimeRecorder.Dispose();
			m_DrawcallsRecorder.Dispose();
			m_SetPassCallsRecorder.Dispose();
			m_TrianglesRecorder.Dispose();
			m_SystemMemoryRecorder.Dispose();
			m_GCMemoryRecorder.Dispose();
		}

	#endregion

	#region Events

		private void OnLevelStarted() => LevelSetup();

		private void OnLevelFinished()
		{
			if (m_RecordingLevel)
				LevelSave();

			m_SaveReportButton.gameObject.SetActive(true);
		}

	#endregion
		

	#region Update
		private void FixedUpdate()
		{
			if (!Debug.isDebugBuild)
				return;

			if (m_RecordingLevel && Time.timeSinceLevelLoad - m_TimeToAddEntry > m_ReportFrequency)
				addEntry();

			if (Input.GetKeyDown(KeyCode.W) && !Input.GetKey(KeyCode.LeftShift))
				Debug.LogWarning("Test Warning");
			if (Input.GetKeyDown(KeyCode.W) && Input.GetKey(KeyCode.LeftShift))
				Debug.LogError("Test error");
		}

	#endregion


	#region Data Setup

		private void InitialSetup()
		{
			m_Data.GlobalInfo.Set(
								Time.realtimeSinceStartup,
								SystemMemory,
								GCMemory);

			m_ValidationWarnings.ForEach(warning => addGlobalMessageEntry(ref m_Data.GlobalInfo.Warnings, new QA_LogMessages(warning, "Project Validator").ToJSon()));
			m_ValidateErrors.ForEach(error => addGlobalMessageEntry(ref m_Data.GlobalInfo.Errors,         new QA_LogMessages(error,   "Project Validator").ToJSon()));

			m_Data.BuildInfo.Set(
							   Application.productName,
							   Application.identifier,
							   Application.version,
							   Application.unityVersion,
							   m_QAReportVersion
							  );

			m_Data.DeviceInfo.Set(
								SystemInfo.operatingSystem,
								SystemInfo.processorType,
								SystemInfo.systemMemorySize.ToString(),
								SystemInfo.graphicsDeviceName,
								SystemInfo.graphicsMemorySize.ToString(),
								SystemInfo.graphicsDeviceType.ToString(),
								Screen.height + " / "                                                    + Screen.width,
								"~"           + Math.Round(new ScreenData().ScreenDiagonalSizeInches, 1) + "\"");
			//If last line gives errors, add something this to ScreenData
			//    public float ScreenDiagonalSizeInches
			//{
			//    get
			//    {
			//        CalculateData();
			//        return m_ScreenSizeInch.magnitude;
			//    }
			//}
		}

		private void LevelSetup()
		{
			m_RecordingLevel = true;

			m_LastTime = m_TimeToAddEntry = Time.timeSinceLevelLoad;

			m_TempLevelInfo = new QA_LevelInfo();
		}

		private void LevelSave()
		{
			m_RecordingLevel = false;

			m_TempLevelInfo.TimePlaying.SetUniqueEntry(new QA_LevelDataEntry(eLevelDataEntry.SingleEntry,
																		   (Time.timeSinceLevelLoad - m_LastTime).ToString(QAReportStaticValues.s_CultureInfo)));

			int currLevelNumber = StorageManager.Instance.CurrentLevel;

			if (!m_Data.LevelInfo.ContainsKey(currLevelNumber))
				m_Data.LevelInfo.Add(currLevelNumber, m_TempLevelInfo);
			else
				m_Data.LevelInfo[currLevelNumber].Merge(m_TempLevelInfo);
		}

	#endregion

	#region Adding Entries

		private void addEntry()
		{
			m_TimeToAddEntry = Time.timeSinceLevelLoad;

			m_TempLevelInfo.FPS.AddEntry(new QA_LevelDataEntry(eLevelDataEntry.Values,     Profiler_FPS.ToString(QAReportStaticValues.s_CultureInfo),     m_TimeStamp, m_PlayerPos));
			m_TempLevelInfo.Batches.AddEntry(new QA_LevelDataEntry(eLevelDataEntry.Values, Profiler_Batches.ToString(QAReportStaticValues.s_CultureInfo), m_TimeStamp, m_PlayerPos));
			m_TempLevelInfo.SetPassCalls.AddEntry(new QA_LevelDataEntry(eLevelDataEntry.Values, Profiler_SetPassCalls.ToString(QAReportStaticValues.s_CultureInfo), m_TimeStamp,
																	  m_PlayerPos));
			m_TempLevelInfo.Triangles.AddEntry(new QA_LevelDataEntry(eLevelDataEntry.Values, Profiler_Triangles.ToString(QAReportStaticValues.s_CultureInfo), m_TimeStamp,
																   m_PlayerPos));
			m_TempLevelInfo.SystemMemory.AddEntry(new QA_LevelDataEntry(eLevelDataEntry.Values, Profiler_SystemMemory.ToString(QAReportStaticValues.s_CultureInfo), m_TimeStamp,
																	  m_PlayerPos));

			//TODO - what about GC spikes?
			m_TempLevelInfo.GC.AddEntry(new QA_LevelDataEntry(eLevelDataEntry.Values, Profiler_GCMemory.ToString(QAReportStaticValues.s_CultureInfo), m_TimeStamp, m_PlayerPos));
		}

		private void addGlobalMessageEntry(ref QA_LevelDataEntries i_List, string i_MessageJSON)
		{
			i_List.AddEntry(new QA_LevelDataEntry(eLevelDataEntry.Messages, i_MessageJSON, Time.timeSinceLevelLoad.ToString(QAReportStaticValues.s_CultureInfo), "",
												  StorageManager.Instance.CurrentLevel.ToString()));
		}

		private void logMessagesCallback(string condition, string stackTrace, LogType type)
		{
			string messageJSON = new QA_LogMessages(condition, stackTrace).ToJSon();

			switch (type)
			{
				case LogType.Error:
				case LogType.Exception:
				case LogType.Assert:
					if (m_RecordingLevel)
						m_TempLevelInfo.Errors.AddEntry(new QA_LevelDataEntry(eLevelDataEntry.Messages, messageJSON, m_TimeStamp, m_PlayerPos));
					else
						addGlobalMessageEntry(ref m_Data.GlobalInfo.Errors, messageJSON);
					break;

				case LogType.Warning:
					if (m_RecordingLevel)
						m_TempLevelInfo.Warnings.AddEntry(new QA_LevelDataEntry(eLevelDataEntry.Messages, messageJSON, m_TimeStamp, m_PlayerPos));
					else
						addGlobalMessageEntry(ref m_Data.GlobalInfo.Warnings, messageJSON);
					break;

				default:
					break;
			}
		}

	#endregion

	#region Export

		[Button]
		private void exportData()
		{
			var jsonString = JsonUtility.ToJson(m_Data);

			if (!Directory.Exists(QAConstants.JSONFolderPath))
				Directory.CreateDirectory(QAConstants.JSONFolderPath);

			using (StreamWriter writer = new StreamWriter(QAConstants.JSONFolderPath + FileName, false))
			{
				writer.WriteLine(jsonString);
				writer.Flush();
			}

			Debug.LogError("Saved Report at - " + QAConstants.JSONFolderPath + FileName);

			StartCoroutine(sendToSlack(jsonString));
		}

		private IEnumerator sendToSlack(string i_JSONString)
		{
			WWWForm form = new WWWForm();

			form.AddField("channelId", "GGSMDN887");
			form.AddField("bundleId",  m_Data.BuildInfo.BundleID);
			form.AddField("title",     FileName);
			form.AddField("fileName",  FileName);
			form.AddField("text",      i_JSONString);

			using (UnityWebRequest www = UnityWebRequest.Post("https://webtools.kobgames.com/projects-init?token=2A2be3cm3e4411&state=post", form))
			{
				yield return www.SendWebRequest();

				if (www.result == UnityWebRequest.Result.Success)
					m_SaveReportButton.gameObject.SetActive(false);
			}
		}

	#endregion

	#region Misc

		private double getRecorderFrameAverage(ProfilerRecorder recorder)
		{
			var samplesCount = recorder.Capacity;
			if (samplesCount == 0)
				return 0;

			double r = 0;
			unsafe
			{
				var samples = stackalloc ProfilerRecorderSample[samplesCount];
				recorder.CopyTo(samples, samplesCount);
				for (var i = 0; i < samplesCount; ++i)
					r += samples[i].Value;
				r /= samplesCount;
			}

			return r;
		}

	#endregion

	}
}