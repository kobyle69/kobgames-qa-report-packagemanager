using System.Globalization;

namespace KobGamesSDKSlim.QAReport
{
    public static class QAReportStaticValues
    {
        private static CultureInfo m_CultureInfo;
        public static CultureInfo s_CultureInfo
        {
            get
            {
                m_CultureInfo ??= CultureInfo.CreateSpecificCulture("en-US");
                return m_CultureInfo;
            }
        }
    }
}