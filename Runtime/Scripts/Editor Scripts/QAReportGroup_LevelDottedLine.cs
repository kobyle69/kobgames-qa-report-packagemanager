using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using System;
using System.Linq;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public enum DottedLineNumberAlignment
    {
        Left,
        Center,
        Right
    }

    public class QAReportGroup_LevelDottedLine
    {
        private VisualElement m_ParentElement;
        private VisualElement m_RowElement; //TODO - don't like this. Used to give margin/padding offset to Dotted Line. Some more straightforward calculation would be better

        private VisualElement m_Element;
        private Label         m_LevelNumberLabelTop;
        private Label         m_LevelValueLabelTop;
        private Label         m_LevelNumberLabelBottom;
        private Label         m_LevelValueLabelBottom;

        //TODO - check flow of this Action. is it ok?
        private Action<QA_Levels_Entries> OnSelect;
        private int                       m_LastLevelSelected = -1;

        public QAReportGroup_LevelDottedLine(VisualElement i_Parent, Action<QA_Levels_Entries> i_OnSelect = null)
        {
            m_ParentElement = i_Parent;
            m_RowElement    = i_Parent.Q<VisualElement>(QAConstants.ElementEntryRowLevel);
            m_Element       = i_Parent.Q<VisualElement>(QAConstants.ElementEntryLevelDottedLine);

            m_LevelNumberLabelTop    = m_Element.Q<Label>(QAConstants.ElementDottedLineLevelTop);
            m_LevelValueLabelTop     = m_Element.Q<Label>(QAConstants.ElementDottedLineValueTop);
            m_LevelNumberLabelBottom = m_Element.Q<Label>(QAConstants.ElementDottedLineLevelBottom);
            m_LevelValueLabelBottom  = m_Element.Q<Label>(QAConstants.ElementDottedLineValueBottom);

            m_Element.visible = false;

            OnSelect = i_OnSelect;
        }

        public void SetVisibility(bool i_Value)
        {
            m_Element.visible = i_Value;
        }

        public void ResetLastLevelSelected()
        {
            m_LastLevelSelected = -1;
        }

        public void Set(Vector3 i_WorldPosition, float i_MouseLocalPosY, float i_Height, QA_Levels_Entries i_Entries, DottedLineNumberAlignment i_NumberAlignement)
        {
            SetAlignment(i_NumberAlignement);

            m_Element.transform.position = i_WorldPosition - Vector3.right * m_Element.resolvedStyle.width * .5f + Vector3.up * (m_RowElement.resolvedStyle.marginTop + m_RowElement.resolvedStyle.paddingTop);
            m_Element.style.height       = i_Height;

            m_LevelNumberLabelTop.text = m_LevelNumberLabelBottom.text = i_Entries.Level.ToString();
            if(i_Entries.DataEntries.Count > 0 && i_Entries.DataEntries.First().EntryType == eLevelDataEntry.SingleEntry)
            {
                m_LevelValueLabelTop.text = m_LevelValueLabelBottom.text = i_Entries.Value.ToTime(true);
            }
            else
            {
                m_LevelValueLabelTop.text = m_LevelValueLabelBottom.text = i_Entries.Value.ShortUpToMillion();
            }

            Vector3 position = new Vector3(m_LevelNumberLabelTop.parent.transform.position.x, i_MouseLocalPosY, m_LevelNumberLabelTop.parent.transform.position.z);
            m_LevelNumberLabelTop.parent.transform.position = position;

            if (m_LastLevelSelected != i_Entries.Level)
            {
                OnSelect?.Invoke(i_Entries);
                m_LastLevelSelected = i_Entries.Level;
            }
        }

        private void SetAlignment(DottedLineNumberAlignment i_NumberAlignement)
        {
            m_LevelNumberLabelTop.style.display = i_NumberAlignement == DottedLineNumberAlignment.Center || i_NumberAlignement == DottedLineNumberAlignment.Right ? DisplayStyle.Flex : DisplayStyle.None;
            m_LevelValueLabelTop.style.display  = i_NumberAlignement == DottedLineNumberAlignment.Center || i_NumberAlignement == DottedLineNumberAlignment.Left ? DisplayStyle.Flex : DisplayStyle.None;

            m_LevelNumberLabelBottom.style.display = i_NumberAlignement == DottedLineNumberAlignment.Left ? DisplayStyle.Flex : DisplayStyle.None;
            m_LevelValueLabelBottom.style.display  = i_NumberAlignement == DottedLineNumberAlignment.Right ? DisplayStyle.Flex : DisplayStyle.None;

            //TODO - not that pretty. should we improve it?
            if (i_NumberAlignement == DottedLineNumberAlignment.Right)
            {
                float deltaPos = m_LevelValueLabelBottom.layout.position.y;

                m_LevelNumberLabelTop.transform.position   = Vector3.up * deltaPos;
                m_LevelValueLabelBottom.transform.position = -Vector3.up * deltaPos;
            }
            else
            {
                m_LevelNumberLabelTop.transform.position = m_LevelValueLabelBottom.transform.position = Vector3.zero;
            }
        }
    }
#endif
}