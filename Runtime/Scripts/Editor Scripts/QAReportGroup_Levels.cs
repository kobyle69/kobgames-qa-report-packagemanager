using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;
using System.Reflection;
using System.Linq;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public class QAReportGroup_Levels : QAReportGroup
    {
        public QAReportGroup_Levels(string i_Title, VisualElement i_RootElement, string i_ParentElementToSearch, float i_FlexGrow = 0)
            : base(i_Title, i_RootElement, i_ParentElementToSearch, i_FlexGrow)
        {
        }

        public override void CreateFields(FieldInfo[] i_FieldInfo, object i_ClassData)
        {
            DictionaryLevelNumberLevelInfo data = (DictionaryLevelNumberLevelInfo)i_ClassData;

            for (int i = 0; i < i_FieldInfo.Length; i++)
            {
                QAReportGroup_LevelGroup group = new QAReportGroup_LevelGroup(m_GroupEntriesElement, i_FieldInfo[i], data);
            }
        }
    }
#endif
}