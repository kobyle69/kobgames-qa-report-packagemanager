using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;
using System.Reflection;
using System.Linq;
using System.Globalization;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public class QAReportGroup_LevelGroup
    {
        protected VisualTreeAsset m_GroupVisualTree;

        private VisualElement m_GroupElement;
        private Label         m_TitleElement;

        private QAReportGroup_LevelDottedLine m_DottedLine;

        public QAReportGroup_LevelGroup(VisualElement i_ParentElement, FieldInfo i_FieldInfo, DictionaryLevelNumberLevelInfo i_Data)
        {
            m_GroupVisualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(QAConstants.GroupEntryLevelUXMLPath);

            m_GroupElement = m_GroupVisualTree.Instantiate();
            m_TitleElement = m_GroupElement.Q<Label>(QAConstants.ElementEntryTitle);
            m_DottedLine   = new QAReportGroup_LevelDottedLine(m_GroupElement);

            i_ParentElement.Add(m_GroupElement);

            string title = i_FieldInfo.Name.SpaceWhenCapitalOrSymbol();
            m_TitleElement.text = title;

            eLevelDataEntry dataEntryType     = ((QA_LevelDataEntries)i_FieldInfo.GetValue(i_Data.FirstOrDefault().Value)).FirstEntry.EntryType;
            string          defaultGraphTitle = dataEntryType == eLevelDataEntry.Values ? "AVERAGE" : "";
            QA_GraphData    averageGraphData  = new QA_GraphData() { Entries = new List<QA_Levels_Entries>(), GraphType = defaultGraphTitle, GraphSection = title, FieldName = i_FieldInfo.Name };
            QA_GraphData    minGraphData      = new QA_GraphData() { Entries = new List<QA_Levels_Entries>(), GraphType = "MIN", GraphSection             = title, FieldName = i_FieldInfo.Name };
            QA_GraphData    maxGraphData      = new QA_GraphData() { Entries = new List<QA_Levels_Entries>(), GraphType = "MAX", GraphSection             = title, FieldName = i_FieldInfo.Name };



            foreach (var item in i_Data)
            {
                List<QA_LevelDataEntry> entries = ((QA_LevelDataEntries)i_FieldInfo.GetValue(item.Value)).Entries;

                float averageValue = 0;
                float minValue     = float.MaxValue;
                float maxValue     = float.MinValue;

                if (entries.Count > 0)
                {
                    //In case there are no entries we get default value. When we do we get the correct value
                    dataEntryType = entries[0].EntryType;

                    switch (dataEntryType)
                    {
                        case eLevelDataEntry.SingleEntry:
                            averageValue = entries.Average(x => float.Parse(x.Value, QAReportStaticValues.s_CultureInfo));
                            break;
                        case eLevelDataEntry.Messages:
                            averageValue = entries.Count;
                            break;
                        case eLevelDataEntry.Values:
                            averageValue = entries.Select(x => float.Parse(x.Value, QAReportStaticValues.s_CultureInfo)).Average();
                            minValue     = entries.Select(x => float.Parse(x.Value, QAReportStaticValues.s_CultureInfo)).Min();
                            maxValue     = entries.Select(x => float.Parse(x.Value, QAReportStaticValues.s_CultureInfo)).Max();
                            break;
                        default:
                            break;
                    }
                }

                averageGraphData.Entries.Add(new QA_Levels_Entries(item.Key, averageValue, entries));
                minGraphData.Entries.Add(new QA_Levels_Entries(item.Key, minValue, dataEntryType == eLevelDataEntry.Messages ? entries : entries.OrderBy(x => float.Parse(x.Value, QAReportStaticValues.s_CultureInfo)).ToList()));
                maxGraphData.Entries.Add(new QA_Levels_Entries(item.Key, maxValue, dataEntryType == eLevelDataEntry.Messages ? entries : entries.OrderByDescending(x => float.Parse(x.Value, QAReportStaticValues.s_CultureInfo)).ToList()));
            }

            averageGraphData.Normalize();
            minGraphData.Normalize();
            maxGraphData.Normalize();

            QA_GraphDataGroup GraphGroup = new QA_GraphDataGroup(0);

            //We will be checking which is the Min/Max of all levels
            float mostMinValue = 0;
            float mostMaxValue = 0;

            if (dataEntryType == eLevelDataEntry.Values)
            {
                GraphGroup.AddGraph(averageGraphData);
                GraphGroup.AddGraph(minGraphData);
                GraphGroup.AddGraph(maxGraphData);

                QAReportGroup_LevelGraph graph  = new QAReportGroup_LevelGraph(m_GroupElement.Q<VisualElement>(QAConstants.ElementEntryLevelAverageValue), GraphGroup, m_DottedLine);
                QAReportGroup_LevelGraph graph2 = new QAReportGroup_LevelGraph(m_GroupElement.Q<VisualElement>(QAConstants.ElementEntryLevelMinValue), GraphGroup.SetGraphID(1), m_DottedLine);
                QAReportGroup_LevelGraph graph3 = new QAReportGroup_LevelGraph(m_GroupElement.Q<VisualElement>(QAConstants.ElementEntryLevelMaxValue), GraphGroup.SetGraphID(2), m_DottedLine);

                mostMinValue = minGraphData.Entries.Min(x => x.Value);
                mostMaxValue = maxGraphData.Entries.Max(x => x.Value);

            }
            else
            {
                GraphGroup.AddGraph(averageGraphData);
                QAReportGroup_LevelGraph graph = new QAReportGroup_LevelGraph(m_GroupElement.Q<VisualElement>(QAConstants.ElementEntryLevelAverageValue), GraphGroup, m_DottedLine);

                m_GroupElement.Q<VisualElement>(QAConstants.ElementEntryLevelMinValue).style.display = DisplayStyle.None;
                m_GroupElement.Q<VisualElement>(QAConstants.ElementEntryLevelMaxValue).style.display = DisplayStyle.None;

                mostMinValue = averageGraphData.Entries.Min(x => x.Value);
                mostMaxValue = averageGraphData.Entries.Max(x => x.Value);
            }

            if(dataEntryType == eLevelDataEntry.SingleEntry && ((QA_LevelDataEntries)i_FieldInfo.GetValue(i_Data.FirstOrDefault().Value)).Entries.Count > 0)
            {
                m_GroupElement.Q<Label>(QAConstants.ElementEntryLevelMinMaxValues).text = mostMinValue.ToTime(true) + " - " + mostMaxValue.ToTime(true);
            }
            else
            {
                m_GroupElement.Q<Label>(QAConstants.ElementEntryLevelMinMaxValues).text = mostMinValue.ShortUpToMillion() + " - " + mostMaxValue.ShortUpToMillion();
            }
        }
    }
#endif
}