using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using System.Linq;
using System.Reflection;
using UnityEditor;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public class QAReportGroup_LevelGraph
    {
        private VisualElement m_GraphParentElement;
        private VisualElement m_GraphBackgroundElement;
        private VisualElement m_GraphBackgroundGreenElement;
        private VisualElement m_GraphBackgroundOrangeElement;
        private VisualElement m_GraphBackgroundRedElement;

        private Label m_GraphTypeLabel;


        private QAReportGroup_LevelDottedLine m_DottedLine;

        private QA_GraphDataGroup m_GraphDataGroup;
        public  QA_GraphData      m_CurrentGraph => m_GraphDataGroup.CurrGraph;

        private float m_MaxValue;
        private int   m_AmountOfLevels => m_CurrentGraph.Entries.Count - 1;

        private List<MeshWriteData> m_UsedMeshes   = new List<MeshWriteData>();
        private List<MeshWriteData> m_PooledMeshes = new List<MeshWriteData>();


        public QAReportGroup_LevelGraph(VisualElement i_GraphBackgroundElement, QA_GraphDataGroup i_GraphDataGroup, QAReportGroup_LevelDottedLine i_DottedLine, bool i_Interactable = true)
        {
            m_GraphParentElement           = i_GraphBackgroundElement;
            m_GraphBackgroundElement       = i_GraphBackgroundElement.Q<VisualElement>(QAConstants.ElementEntryLevelBackground);
            m_GraphBackgroundGreenElement  = i_GraphBackgroundElement.Q<VisualElement>(QAConstants.ElementEntryLevelBackgroundGreen);
            m_GraphBackgroundOrangeElement = i_GraphBackgroundElement.Q<VisualElement>(QAConstants.ElementEntryLevelBackgroundOrange);
            m_GraphBackgroundRedElement    = i_GraphBackgroundElement.Q<VisualElement>(QAConstants.ElementEntryLevelBackgroundRed);
            m_GraphTypeLabel               = i_GraphBackgroundElement.Q<Label>(QAConstants.ElementEntryLevelGraphTypeLabel);

            if (i_Interactable)
                m_GraphBackgroundElement.styleSheets.Add(QAReportWindow_Editor.SelectorButtonStyleSheet);

            SetData(i_GraphDataGroup);
            m_DottedLine = i_DottedLine;


            m_GraphBackgroundElement.RegisterCallback<MouseOverEvent>((evt) => { m_DottedLine.SetVisibility(true); });
            m_GraphBackgroundElement.RegisterCallback<MouseOutEvent>((evt) => { m_DottedLine.SetVisibility(false); });
            m_GraphBackgroundElement.RegisterCallback<MouseMoveEvent>(OnMouseDrag);

            QAReportWindow_Editor.OnGraphBackgroundUpdate += UpdateGraphBackground;

            if (i_Interactable)
                m_GraphBackgroundElement.RegisterCallback<MouseDownEvent>((evt) => { QAReportWindow_Editor.Instance.OpenWindowLevelDetails(i_GraphDataGroup); });

            m_GraphBackgroundElement.generateVisualContent += GenerateGraph;

            m_GraphBackgroundElement.RegisterCallback<GeometryChangedEvent>((evt) =>
            {
                UpdateGraphBackground();

                //TODO - it would be nice to have this controled by the Template instead of code
                m_GraphTypeLabel.transform.position =
                    Vector3.up * (m_GraphBackgroundElement.resolvedStyle.height * .5f - m_GraphTypeLabel.resolvedStyle.height * .5f) +
                    Vector3.right * (m_GraphBackgroundElement.resolvedStyle.width * .5f - m_GraphTypeLabel.resolvedStyle.width * .5f);
            });
        }

        ~QAReportGroup_LevelGraph()
        {
            QAReportWindow_Editor.OnGraphBackgroundUpdate -= UpdateGraphBackground;
        }

        public void SetData(QA_GraphDataGroup i_GraphDataGroup)
        {
            m_GraphDataGroup      = i_GraphDataGroup;
            m_GraphTypeLabel.text = m_CurrentGraph.GraphType;
            if (m_CurrentGraph.Entries.Count > 0)
                m_MaxValue = m_CurrentGraph.Entries.Select(d => d.Value).Max();

            m_GraphBackgroundElement.MarkDirtyRepaint();
            UpdateGraphBackground();
        }

        public void UpdateGraphBackground()
        {
            m_GraphBackgroundGreenElement.BringToFront();
            m_GraphBackgroundRedElement.SendToBack();

            m_GraphBackgroundRedElement.style.display    = DisplayStyle.Flex;
            m_GraphBackgroundOrangeElement.style.display = DisplayStyle.Flex;
            m_GraphBackgroundGreenElement.style.display  = DisplayStyle.Flex;

            //TODO - should be changed in the future so we don't need to have the prefab on the scene
            var                   marginsGUID = AssetDatabase.FindAssets("t:" + nameof(QAReportTargets)).First();
            var                   margins     = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(marginsGUID), typeof(QAReportTargets)) as QAReportTargets;
            QA_Level_TargetValues Margins     = margins.GetValues(m_CurrentGraph.FieldName);

            //TODO - simplify code
            if (!Margins.InvertGraph)
            {
                if (m_MaxValue > Margins.RedlineValue)
                {
                    m_GraphBackgroundRedElement.style.flexGrow = 0;

                    m_GraphBackgroundRedElement.style.display = DisplayStyle.Flex;
                    m_GraphBackgroundRedElement.style.height  = (m_MaxValue - Margins.RedlineValue) / m_MaxValue * m_GraphBackgroundElement.resolvedStyle.height;
                }
                else
                    m_GraphBackgroundRedElement.style.display = DisplayStyle.None;

                if (m_MaxValue > Margins.GreenLineValue)
                {
                    float maxValueCapped = Mathf.Min(m_MaxValue, Margins.RedlineValue);

                    m_GraphBackgroundOrangeElement.style.display = DisplayStyle.Flex;
                    m_GraphBackgroundOrangeElement.style.height  = (maxValueCapped - Margins.GreenLineValue) / m_MaxValue * m_GraphBackgroundElement.resolvedStyle.height;
                }
                else
                    m_GraphBackgroundOrangeElement.style.display = DisplayStyle.None;

                m_GraphBackgroundGreenElement.style.flexGrow = 1;
            }
            else
            {
                m_GraphBackgroundGreenElement.SendToBack();
                m_GraphBackgroundRedElement.BringToFront();

                if (m_MaxValue > Margins.GreenLineValue)
                {
                    m_GraphBackgroundGreenElement.style.flexGrow = 0;

                    m_GraphBackgroundGreenElement.style.display = DisplayStyle.Flex;
                    m_GraphBackgroundGreenElement.style.height  = (m_MaxValue - Margins.GreenLineValue) / m_MaxValue * m_GraphBackgroundElement.resolvedStyle.height;
                }
                else
                    m_GraphBackgroundGreenElement.style.display = DisplayStyle.None;

                if (m_MaxValue > Margins.RedlineValue)
                {
                    float maxValueCapped = Mathf.Min(m_MaxValue, Margins.GreenLineValue);

                    m_GraphBackgroundOrangeElement.style.display = DisplayStyle.Flex;
                    m_GraphBackgroundOrangeElement.style.height  = (maxValueCapped - Margins.RedlineValue) / m_MaxValue * m_GraphBackgroundElement.resolvedStyle.height;
                }
                else
                    m_GraphBackgroundGreenElement.style.display = DisplayStyle.None;

                m_GraphBackgroundRedElement.style.flexGrow = 1;
            }
        }

        private void OnMouseDrag(MouseMoveEvent i_event)
        {
            float width              = m_GraphBackgroundElement.resolvedStyle.width;
            float spaceBetweenLevels = width / (float)m_AmountOfLevels;
            int   closeLevel         = Mathf.Max(0, Mathf.RoundToInt(i_event.localMousePosition.x / spaceBetweenLevels));

            Vector3 finalPos = Vector3.right * (closeLevel * spaceBetweenLevels);
            finalPos += Vector3.right * m_GraphBackgroundElement.worldBound.x;

            DottedLineNumberAlignment dottedLineNumberAlignement = DottedLineNumberAlignment.Center;
            if (closeLevel == 0)
                dottedLineNumberAlignement = DottedLineNumberAlignment.Left;
            else if (closeLevel == m_CurrentGraph.Entries.Count - 1)
                dottedLineNumberAlignement = DottedLineNumberAlignment.Right;

            m_DottedLine.Set(
                finalPos,
                i_event.localMousePosition.y,
                m_GraphBackgroundElement.resolvedStyle.height,
                m_CurrentGraph.Entries[closeLevel],
                dottedLineNumberAlignement);
        }

        private void GenerateGraph(MeshGenerationContext context)
        {
            List<Vector3> Points = new List<Vector3>();
            List<Vector3> Area   = new List<Vector3>();


            float width  = m_GraphBackgroundElement.resolvedStyle.width;
            float height = m_GraphBackgroundElement.resolvedStyle.height;

            //Debug.LogError("Width " + width);
            //Debug.LogError("Height " + height);


            for (int i = 0; i < m_CurrentGraph.Entries.Count; i++)
            {
                float xPos = width * (i / ((float)m_CurrentGraph.Entries.Count - 1));
                float yPos = height - (m_CurrentGraph.Entries[i].Value / m_MaxValue * height);

                //Debug.LogError("X Pos " + xPos);

                Points.Add(Vector3.right * xPos + Vector3.up * yPos);
            }

            Area.InsertRange(0, Points);
            Area.Add(Vector3.right * width + Vector3.up * height);
            Area.Add(Vector3.up * height);
            Area.Add(Area[0]);


            DrawArea(Area.ToArray(), 2, Color.white * .5f, context);
            DrawLines(Points.ToArray(), 2, Color.white, context);
        }

        private void DrawLines(Vector3[] points, float thickness, Color color, MeshGenerationContext context)
        {
            List<Vertex> vertices = new List<Vertex>();
            List<ushort> indices  = new List<ushort>();

            for (int i = 0; i < points.Length - 1; i++)
            {
                var pointA = points[i];
                var pointB = points[i + 1];

                float angle   = Mathf.Atan2(pointB.y - pointA.y, pointB.x - pointA.x);
                float offsetX = thickness / 2 * Mathf.Sin(angle);
                float offsetY = thickness / 2 * Mathf.Cos(angle);

                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointA.x + offsetX, pointA.y - offsetY, Vertex.nearZ),
                    tint     = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointB.x + offsetX, pointB.y - offsetY, Vertex.nearZ),
                    tint     = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointB.x - offsetX, pointB.y + offsetY, Vertex.nearZ),
                    tint     = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointB.x - offsetX, pointB.y + offsetY, Vertex.nearZ),
                    tint     = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointA.x - offsetX, pointA.y + offsetY, Vertex.nearZ),
                    tint     = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointA.x + offsetX, pointA.y - offsetY, Vertex.nearZ),
                    tint     = color
                });

                ushort indexOffset(int value) => (ushort)(value + (i * 6));
                indices.Add(indexOffset(0));
                indices.Add(indexOffset(1));
                indices.Add(indexOffset(2));
                indices.Add(indexOffset(3));
                indices.Add(indexOffset(4));
                indices.Add(indexOffset(5));
            }

            CreateMesh(context, vertices, indices);
        }

        private void DrawArea(Vector3[] points, float thickness, Color color, MeshGenerationContext context)
        {
            List<Vertex> vertices = new List<Vertex>();
            List<ushort> indices  = new List<ushort>();

            for (int i = 0; i < points.Length - 1; i++)
            {
                var pointA = points[i];
                var pointB = points[i + 1];

                float angle   = Mathf.Atan2(pointB.y - pointA.y, pointB.x - pointA.x);
                float offsetX = thickness / 2 * Mathf.Sin(angle);
                float offsetY = thickness / 2 * Mathf.Cos(angle);

                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointA.x + offsetX, pointA.y - offsetY, Vertex.nearZ),
                    tint     = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointB.x + offsetX, pointB.y - offsetY, Vertex.nearZ),
                    tint     = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointB.x - offsetX, pointB.y + offsetY, Vertex.nearZ),
                    tint     = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointB.x - offsetX, pointB.y + offsetY, Vertex.nearZ),
                    tint     = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointA.x - offsetX, pointA.y + offsetY, Vertex.nearZ),
                    tint     = color
                });
                vertices.Add(new Vertex()
                {
                    position = new Vector3(pointA.x + offsetX, pointA.y - offsetY, Vertex.nearZ),
                    tint     = color
                });

                ushort indexOffset(int value) => (ushort)(value + (i * 6));
                indices.Add(indexOffset(0));
                indices.Add(indexOffset(1));
                indices.Add(indexOffset(2));
                indices.Add(indexOffset(3));
                indices.Add(indexOffset(4));
                indices.Add(indexOffset(5));
            }

            CreateMesh(context, vertices, indices);
        }

        private void CreateMesh(MeshGenerationContext context, List<Vertex> i_Vertices, List<ushort> i_Indices)
        {
            MeshWriteData mesh;

            if (m_PooledMeshes.Count > 0)
            {
                mesh = m_PooledMeshes[0];
                m_PooledMeshes.RemoveAt(0);
                m_UsedMeshes.Add(mesh);
            }
            else
                mesh = context.Allocate(i_Vertices.Count, i_Indices.Count);

            mesh.SetAllVertices(i_Vertices.ToArray());
            mesh.SetAllIndices(i_Indices.ToArray());
        }
    }
#endif
}