using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;
using System.Reflection;
using System.Linq;
using System;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public class QAReportGroup_LevelDetailsGraph : QAReportGroup
    {
        protected VisualTreeAsset m_LevelDetailsGraphVisualTree;

        private VisualElement m_LevelDetailsGraphElement;

        private QAReportGroup_LevelDottedLine m_DottedLine;

        private QAReportGroup_LevelGraph m_Graph;


        public QA_GraphData CurrGraph => m_Graph.m_CurrentGraph;



        private VisualElement m_ElementToCheckForVisibility => m_ParentElement;
        public  bool          IsVisible                     => m_ElementToCheckForVisibility.style.display == DisplayStyle.Flex;


        public QAReportGroup_LevelDetailsGraph(string i_Title, VisualElement i_RootElement, string i_ParentElementToSearch, Action<QA_Levels_Entries> i_OnLineSelect, float i_FlexGrow = 0)
            : base(i_Title, i_RootElement, i_ParentElementToSearch, i_FlexGrow)
        {
            m_LevelDetailsGraphVisualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(QAConstants.LevelDetailsGraphUXMLPath);

            m_LevelDetailsGraphElement = m_LevelDetailsGraphVisualTree.Instantiate();

            m_DottedLine = new QAReportGroup_LevelDottedLine(m_LevelDetailsGraphElement, i_OnLineSelect);

            m_GroupEntriesElement.Add(m_LevelDetailsGraphElement);

            m_Graph = new QAReportGroup_LevelGraph(m_LevelDetailsGraphElement.Q<VisualElement>(QAConstants.ElementGroupEntries), QA_GraphDataGroup.GetDefault(), m_DottedLine, false);
        }

        public override void CreateFields(FieldInfo[] i_FieldInfo, object i_ClassData)
        {
            //Not Used
        }

        public void SetData(QA_GraphDataGroup i_GraphDataGroup)
        {
            //m_TitleElement.text = i_GraphData.GraphSection.ToUpper() + QAReportWindow_Editor.DataLevelRange;

            float minValue = i_GraphDataGroup.CurrGraph.Entries.Select(d => d.Value).Min();
            float maxValue = i_GraphDataGroup.CurrGraph.Entries.Select(d => d.Value).Max();

            if (i_GraphDataGroup.CurrGraph.Entries.Count > 0 && i_GraphDataGroup.CurrGraph.Entries.First().DataEntries.Count > 0 && 
                i_GraphDataGroup.CurrGraph.Entries.First().DataEntries.First().EntryType == eLevelDataEntry.SingleEntry)
            {
                m_TitleElement.text = i_GraphDataGroup.CurrGraph.GraphSection.ToUpper() + "    (" + minValue.ToTime(true) + " - " + maxValue.ToTime(true) + ")";
            }
            else
            {
                m_TitleElement.text = i_GraphDataGroup.CurrGraph.GraphSection.ToUpper() + "    (" + minValue.ShortUpToMillion() + " - " + maxValue.ShortUpToMillion() + ")";
            }
            
            

            m_Graph.SetData(i_GraphDataGroup);

            m_DottedLine.ResetLastLevelSelected();
        }

        public void SetVisibility(bool i_Visible)
        {
            m_ElementToCheckForVisibility.style.display = i_Visible ? DisplayStyle.Flex : DisplayStyle.None;
        }

    }
#endif
}