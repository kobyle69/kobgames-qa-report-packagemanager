using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;
using System.Reflection;
using System;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public class QAReportGroup_Labels : QAReportGroup
    {
        //TODO - is it good idea to have i_RootElement and i_ParentElementToSearch instead of providing the element directly?
        public QAReportGroup_Labels(string i_Title, VisualElement i_RootElement, string i_ParentElementToSearch, float i_FlexGrow = 0)
            : base(i_Title, i_RootElement, i_ParentElementToSearch, i_FlexGrow)
        {
        }

        public override void CreateFields(FieldInfo[] i_FieldInfo, object i_ClassData)
        {
            for (int i = 0; i < i_FieldInfo.Length; i++)
            {
                string textValue = "";

                if (i_FieldInfo[i].FieldType == typeof(QA_LevelDataEntries))
                {
                    QA_LevelDataEntries valuesList = ((QA_LevelDataEntries)i_FieldInfo[i].GetValue(i_ClassData));
                    textValue = valuesList.Entries.Count.ToString();

                    CreateLabelWithStringList(i_FieldInfo[i].Name, textValue, valuesList);
                }
                else
                {
                    textValue = i_FieldInfo[i].GetValue(i_ClassData).ToString();

                    float parsedValue;
                    if (float.TryParse(textValue, out parsedValue))
                        textValue = Mathf.Round(parsedValue).ToString();

                    CreateLabel(i_FieldInfo[i].Name, i_FieldInfo[i].Name == nameof(QA_GlobalInfo.LoadingTime) ? textValue.ToTime() : textValue);
                }
            }
        }

        public void CreateLabel(string i_Name, string i_Value)
        {
            var element = m_EntryVisualTree.Instantiate();

            m_GroupEntriesElement.Add(element);

            element.Q<Label>(QAConstants.ElementEntryTitle).text = i_Name.SpaceWhenCapitalOrSymbol();
            element.Q<Label>(QAConstants.ElementEntryValue).text = i_Value;
        }

        public void CreateLabelWithStringList(string i_Name, string i_Value, QA_LevelDataEntries i_Values)
        {
            var element = m_EntryVisualTree.Instantiate();

            m_GroupEntriesElement.Add(element);

            element.Q<Label>(QAConstants.ElementEntryTitle).text = i_Name.SpaceWhenCapitalOrSymbol();

            Label valueLable = element.Q<Label>(QAConstants.ElementEntryValue);
            valueLable.text                          = i_Value;
            valueLable.style.unityFontStyleAndWeight = FontStyle.BoldAndItalic;
            valueLable.styleSheets.Add(QAReportWindow_Editor.SelectorButtonStyleSheet);

            valueLable.RegisterCallback<MouseDownEvent>(
                (evt) =>
                {
                    //TODO - should we ignore click if there is no values?
                    //if(i_Values.Entries.Count > 0)
                    QAReportWindow_Editor.Instance.OpenWindowLevelDetails(i_Name, i_Values);
                }
            );
        }
    }
#endif
}