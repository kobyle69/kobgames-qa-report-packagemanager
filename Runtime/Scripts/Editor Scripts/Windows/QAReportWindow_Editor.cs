using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR

    public class QAReportWindow_Editor : EditorWindow
    {
        public static QAReportData ReportData = new QAReportData();

        private       StyleSheet m_StyleSheet;
        public static StyleSheet SelectorButtonStyleSheet;

        private VisualTreeAsset m_WindowVisualTree;

        private VisualElement m_EditorWindowElement;
        private Label         m_ElementLoadedJSONLabel;

        private string m_DataPath;

        private QAReportWindow_Main         m_WindowMain;
        private QAReportWindow_LevelDetails m_WindowLevelDetails;

        public static QAReportWindow_Editor Instance;

        public static string DataLevelRange
        {
            get
            {
                List<int> Levels = ReportData.LevelInfo.Select(d => d.Key).ToList();
                return "    (" + Levels.Min() + " - " + Levels.Max() + ")";
            }
        }

        public static Action OnGraphBackgroundUpdate;

        [MenuItem("KobGamesSDK/Tools/QA Report #%&q", false, 500)]
        public static void ShowExample()
        {
            QAReportWindow_Editor wnd = GetWindow<QAReportWindow_Editor>();
            wnd.titleContent = new GUIContent("QA Report");
        }

        public void CreateGUI()
        {
            Instance = this;

            m_StyleSheet             = AssetDatabase.LoadAssetAtPath<StyleSheet>(QAConstants.MainStyleSheetPath);
            SelectorButtonStyleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(QAConstants.SelectorButtonStyleSheetPath);

            m_WindowVisualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(QAConstants.WindowEditorXMLPath);

            m_EditorWindowElement = m_WindowVisualTree.Instantiate();
            m_EditorWindowElement.styleSheets.Add(m_StyleSheet);

            m_EditorWindowElement.style.flexGrow = 1;

            rootVisualElement.Add(m_EditorWindowElement);

            //Button loadButton = m_EditorWindowElement.Q<Button>(QAConstants.ElementLoadButton);
            //loadButton.RegisterCallback<ClickEvent>((evt) => { LoadData(); });

            Label qaVersion = m_EditorWindowElement.Q<Label>(QAConstants.ElementQAVersion);
            m_ElementLoadedJSONLabel = m_EditorWindowElement.Q<Label>(QAConstants.ElementLoadedJSON);
            m_ElementLoadedJSONLabel.RegisterCallback<ClickEvent>((evt) => LoadData() );

            qaVersion.text = GetReadmeVersion();

            //LoadData();
        }



        private void LoadData()
        {
            m_DataPath = EditorUtility.OpenFilePanelWithFilters("Select QA Data Json", QAConstants.JSONFolderPath, new string[] { "JSON", "json", "Text", "txt" });

            //ReportData = QAReport.ImportData(File.ReadAllText("Assets/QA Report/JSON_Test.txt"));

            if (!File.Exists(m_DataPath))
            {
                Debug.LogError("Aborted or File doesn't exist");
                return;
            }

            if (Path.GetExtension(m_DataPath) != ".txt" && Path.GetExtension(m_DataPath) != ".json")
            {
                Debug.LogError("File should be a txt or json. Selected file is - " + Path.GetExtension(m_DataPath));
                return;
            }

            string[] loadedJSONSplit = Path.GetFileName(m_DataPath).Split('_');
            m_ElementLoadedJSONLabel.text = loadedJSONSplit[0];
            if (loadedJSONSplit.Length > 1)
                m_ElementLoadedJSONLabel.text += " - " + loadedJSONSplit[1];


            ReportData = ImportData(File.ReadAllText(m_DataPath));

            var windowMainElement   = m_EditorWindowElement.Q<ScrollView>(QAConstants.ElementWindowMain);
            var levelDetailsElement = m_EditorWindowElement.Q(QAConstants.ElementLevelDetails);
            
            if (m_WindowMain != null)
            {
                windowMainElement.Remove(m_WindowMain.WindowElement);
                levelDetailsElement.Remove(m_WindowLevelDetails.WindowElement);
            }
            
            m_WindowMain         = new QAReportWindow_Main(windowMainElement, QAConstants.WindowMainXMLPath, true);
            m_WindowLevelDetails = new QAReportWindow_LevelDetails(levelDetailsElement, QAConstants.WindowLevelDetailsXMLPath, false, 1);
        }

        public void OpenMainWindow()
        {
            m_WindowLevelDetails.Close();
            m_WindowMain.Open();
        }

        //TODO - this duplicate funcs are strange
        public void OpenWindowLevelDetails(QA_GraphDataGroup i_GraphDataGroup)
        {
            OpenWindowLevelDetails(true);
            m_WindowLevelDetails.SetData(i_GraphDataGroup);
        }

        public void OpenWindowLevelDetails(string i_Type, QA_LevelDataEntries i_DataEntries)
        {
            OpenWindowLevelDetails(false);
            m_WindowLevelDetails.SetData(i_Type, i_DataEntries);
        }

        private void OpenWindowLevelDetails(bool i_ShowGraph = true)
        {
            m_WindowMain.Close();
            m_WindowLevelDetails.Open(i_ShowGraph);
        }

        public void UpdateGraphsBackgrounds()
        {
            OnGraphBackgroundUpdate?.Invoke();
        }
        
        public static QAReportData ImportData(string i_Json)
        {
            return JsonUtility.FromJson<QAReportData>(i_Json);
        }
        
        public static string GetReadmeVersion() 
        {
#if PACKAGE_EDITOR
            string pluginFolderPath = Application.dataPath;
#else
            string pluginFolderPath = new DirectoryInfo(Application.dataPath).Parent + "/Packages";
#endif
            
            string readmePath = pluginFolderPath + "/com.kobgames.qa_report/CHANGELOG.md";

            if (!File.Exists(readmePath))
            {
                Debug.LogError("Directory doesn't exist - " + readmePath);
                return "Error";
            }

            StreamReader reader = new StreamReader(readmePath, true);

            string firstLine = reader.ReadLine();
            // string versionText = firstLine.Split('-')[1].Trim();
            string versionText = firstLine.Replace("#","").Trim();


            return versionText;
        }
    }
#endif
}