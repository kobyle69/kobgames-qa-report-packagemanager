using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public class QAReportWindow
    {
        protected VisualTreeAsset m_WindowVisualTree;

        protected VisualElement m_ParentElement;
        protected VisualElement m_WindowElement;
        public    VisualElement WindowElement => m_WindowElement;

        public QAReportWindow(VisualElement i_ParentElement, string i_WindowVisualTree, bool i_Show, float i_FlexGrow = 0)
        {
            m_ParentElement = i_ParentElement;

            m_WindowVisualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(i_WindowVisualTree);
            m_WindowElement    = m_WindowVisualTree.Instantiate();

            m_ParentElement.Add(m_WindowElement);

            m_ParentElement.style.display  = i_Show ? DisplayStyle.Flex : DisplayStyle.None;
            m_ParentElement.style.flexGrow = m_WindowElement.style.flexGrow = i_FlexGrow;
        }

        public virtual void Open()
        {
            m_ParentElement.style.display = DisplayStyle.Flex;
        }

        public virtual void Close()
        {
            m_ParentElement.style.display = DisplayStyle.None;
        }
    }
#endif
}