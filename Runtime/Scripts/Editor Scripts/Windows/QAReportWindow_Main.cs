using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using System.Reflection;
using System.Linq;
using System;
using UnityEditor;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public class QAReportWindow_Main : QAReportWindow
    {
        private QAReportGroup_Labels m_BuildInfo;
        private QAReportGroup_Labels m_DeviceInfo;
        private QAReportGroup_Labels m_GlobalInfo;
        private QAReportGroup_Levels m_LevelsInfo;

        private QAReportData m_ReportData     => QAReportWindow_Editor.ReportData;
        private string       m_DataLevelRange => m_ReportData.LevelInfo.Count > 0 ? QAReportWindow_Editor.DataLevelRange : " - NOT FOUND";


        public QAReportWindow_Main(ScrollView i_ParentElement, string i_WindowVisualTree, bool i_Show) : base(i_ParentElement, i_WindowVisualTree, i_Show)
        {
            m_BuildInfo  = new QAReportGroup_Labels(QAConstants.SectionBuildInfo,  m_WindowElement, QAConstants.ElementBuildInfoGroup);
            m_DeviceInfo = new QAReportGroup_Labels(QAConstants.SectionDeviceInfo, m_WindowElement, QAConstants.ElementDeviceInfoGroup);
            m_GlobalInfo = new QAReportGroup_Labels(QAConstants.SectionGlobalInfo, m_WindowElement, QAConstants.ElementGlobalInfoGroup);
            m_LevelsInfo = new QAReportGroup_Levels(QAConstants.SectionLevelsInfo + m_DataLevelRange, m_WindowElement, QAConstants.ElementLevelsInfoGroup);


            m_BuildInfo.CreateFields(GetFields(typeof(QA_BuildInfo)), m_ReportData.BuildInfo);
            m_DeviceInfo.CreateFields(GetFields(typeof(QA_DeviceInfo)), m_ReportData.DeviceInfo);
            m_GlobalInfo.CreateFields(GetFields(typeof(QA_GlobalInfo)), m_ReportData.GlobalInfo);

            if (m_ReportData.LevelInfo.Count > 0)
                m_LevelsInfo.CreateFields(GetFields(typeof(QA_LevelInfo)), m_ReportData.LevelInfo);
        }

        private static FieldInfo[] GetFields(Type i_Type)
        {
            List<FieldInfo> allFields = new List<FieldInfo>();

            const BindingFlags flags = BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic;

            allFields.AddRange(i_Type.GetFields(flags));

            return allFields.ToArray();
        }
    }
#endif
}