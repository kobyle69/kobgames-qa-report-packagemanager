using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using System.Reflection;
using System.Linq;
using System;
using UnityEditor;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public class QAReportWindow_LevelDetails : QAReportWindow
    {
        private QAReportGroup_LevelDetailsGraph m_GraphGroup;
        private QAReportGroup_LevelDetails      m_DetailsGroup;

        private Button m_BackButton;
        private Button m_PrevGraphButton;
        private Button m_NextGraphButton;

        //TODO - this would be prettier with a dedicated class
        private VisualElement m_LogMessagesParentElement;
        private Label         m_LogMessagesLogValueLabel;
        private Label         m_LogMessagesStacktraceValueLabel;
        private Button        m_LogCopyButton;

        private VisualElement m_MainSpacer;

        private int m_LastSelectedLevel = 0;


        private bool              m_IsGraphGroupSet = false;
        private QA_GraphDataGroup m_GraphDataGroup;
        private QA_LogMessages    m_SelectedMessage;

        public QAReportWindow_LevelDetails(VisualElement i_ParentElement, string i_WindowVisualTree, bool i_Show, float i_FlexGrow)
            : base(i_ParentElement, i_WindowVisualTree, i_Show, i_FlexGrow)
        {
            m_GraphGroup   = new QAReportGroup_LevelDetailsGraph("Stuff", m_WindowElement, QAConstants.ElementLevelDetailsGraphGroup, OnLevelSelected);
            m_DetailsGroup = new QAReportGroup_LevelDetails("Level ", m_WindowElement, QAConstants.ElementLevelDetailsGroup, 1, OnLogSelected);

            m_BackButton      = m_WindowElement.Q<Button>(QAConstants.ElementBackButton);
            m_PrevGraphButton = m_WindowElement.Q<Button>(QAConstants.ElementPrevButton);
            m_NextGraphButton = m_WindowElement.Q<Button>(QAConstants.ElementNextButton);
            m_MainSpacer      = m_WindowElement.Q<VisualElement>(QAConstants.ElementMainSpacer);

            m_LogMessagesParentElement        = m_ParentElement.Q<VisualElement>(QAConstants.ElementLogMessageDetailsParent);
            m_LogMessagesLogValueLabel        = m_LogMessagesParentElement.Q<Label>(QAConstants.ElementLogMessageLogValue);
            m_LogMessagesStacktraceValueLabel = m_LogMessagesParentElement.Q<Label>(QAConstants.ElementLogMessageStacktraceValue);
            m_LogCopyButton                   = m_LogMessagesParentElement.Q<Button>(QAConstants.ElementLogCopy);

            m_LogMessagesParentElement.style.display = DisplayStyle.None;

            m_BackButton.RegisterCallback<ClickEvent>((evt) => { QAReportWindow_Editor.Instance.OpenMainWindow(); });

            m_LogCopyButton.RegisterCallback<ClickEvent>((evt) => { GUIUtility.systemCopyBuffer = m_SelectedMessage.Log + "\n\n" + m_SelectedMessage.StackTrace; });
        }

        public void Open(bool i_ShowGraph)
        {
            base.Open();

            m_IsGraphGroupSet   = false;
            m_LastSelectedLevel = 0;

            m_GraphGroup.SetVisibility(i_ShowGraph);
            m_PrevGraphButton.style.display = m_NextGraphButton.style.display = m_MainSpacer.style.display = i_ShowGraph ? DisplayStyle.Flex : DisplayStyle.None;

        }

        /// <summary>
        /// Set Data with Graph
        /// </summary>
        /// <param name="i_GraphDataGroup"></param>
        public void SetData(QA_GraphDataGroup i_GraphDataGroup)
        {
            m_IsGraphGroupSet = true;
            m_DetailsGroup.Clear();

            m_GraphDataGroup = i_GraphDataGroup;
            m_GraphGroup.SetData(i_GraphDataGroup);

            if (m_GraphDataGroup.Graphs.Count == 1)
            {
                m_PrevGraphButton.style.display = m_NextGraphButton.style.display = DisplayStyle.None;
            }
            else
            {
                m_PrevGraphButton.style.display = m_NextGraphButton.style.display = DisplayStyle.Flex;

                m_PrevGraphButton.text = " < " + i_GraphDataGroup.PreviousGraph.GraphType;
                m_NextGraphButton.text = i_GraphDataGroup.NextGraph.GraphType + " > ";

                m_PrevGraphButton.RegisterCallback<ClickEvent>((evt) =>
                {
                    m_GraphDataGroup.SetGraphID(m_GraphDataGroup.PreviousGraphID);
                    SetData(m_GraphDataGroup);
                });

                m_NextGraphButton.RegisterCallback<ClickEvent>((evt) =>
                {
                    m_GraphDataGroup.SetGraphID(m_GraphDataGroup.NextGraphID);
                    SetData(m_GraphDataGroup);
                });
            }

            OnLevelSelected(i_GraphDataGroup.CurrGraph.Entries[m_LastSelectedLevel]);
        }

        /// <summary>
        /// Set Data without Graph
        /// </summary>
        /// <param name="i_GroupTitle"></param>
        /// <param name="i_Entries"></param>
        public void SetData(string i_GroupTitle, QA_LevelDataEntries i_Entries)
        {
            m_DetailsGroup.Clear();

            OnLevelSelected("GLOBAL INFO   -   " + i_GroupTitle.ToUpper() + "   -   " + i_Entries.Entries.Count, new QA_Levels_Entries(-1, i_Entries.Entries.Count, i_Entries.Entries));
        }

        public void OnLevelSelected(QA_Levels_Entries i_LevelEntries)
        {
            string graphType = m_GraphGroup.IsVisible ? m_GraphDataGroup.CurrGraph.GraphType : "";
            string title     = "";
            if (i_LevelEntries.DataEntries.Count > 0 && i_LevelEntries.DataEntries.First().EntryType == eLevelDataEntry.SingleEntry)
            {
                title = "LEVEL " + i_LevelEntries.Level + (graphType != "" ? "   -   " + graphType : "") + "   -   " + i_LevelEntries.Value.ToTime();
            }
            else
            {
                title = "LEVEL " + i_LevelEntries.Level + (graphType != "" ? "   -   " + graphType : "") + "   -   " + i_LevelEntries.Value.ShortUpToMillion();
            }
            
            OnLevelSelected(title, i_LevelEntries);
        }

        private void OnLevelSelected(string i_GroupTitle, QA_Levels_Entries i_LevelEntries)
        {
            if (m_IsGraphGroupSet)
                m_LastSelectedLevel = m_GraphDataGroup.CurrGraph.Entries.IndexOf(i_LevelEntries);

            m_DetailsGroup.SetData(i_GroupTitle, i_LevelEntries);

            m_LogMessagesParentElement.style.display = DisplayStyle.None;
        }

        public void OnLogSelected(QA_LogMessages i_Message)
        {
            m_SelectedMessage = i_Message;

            m_LogMessagesParentElement.style.display = DisplayStyle.Flex;

            m_LogMessagesLogValueLabel.text        = i_Message.Log;
            m_LogMessagesStacktraceValueLabel.text = i_Message.StackTrace;
        }
    }
#endif
}