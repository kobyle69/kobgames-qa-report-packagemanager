using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;
using System.Linq;
using KobGamesSDKSlim.QAReport.UI;
using Sirenix.OdinInspector;
using KobGamesSDKSlim;

namespace KobGamesSDKSlim.QAReport
{
#if UNITY_EDITOR

    [System.Serializable]
    public class DictionaryFieldNameTargetValues : UnitySerializedDictionary<string, QA_Level_TargetValues>
    {
    }

    [CreateAssetMenu(fileName = "QA Target Values - ", menuName = "KobGames/QA Target Values")]
    public class QAReportTargets : ScriptableObject
    {
        public DictionaryFieldNameTargetValues TargetValues = new DictionaryFieldNameTargetValues();

        private void OnValidate()
        {
            CorrectDictionary();
        }

        [Button]
        private void CorrectDictionary()
        {
            var isDirty = false;

            var fields = GetFields(typeof(QA_LevelInfo));

            //Remove unused fields
            var itemsToRemove = TargetValues.Keys.Where(item => !Array.Exists(fields, x => x.Name == item)).ToList();

            foreach (var item in itemsToRemove)
            {
                TargetValues.Remove(item);
                isDirty = true;
            }

            //Add missing fields
            foreach (var field in fields)
            {
                if (TargetValues.ContainsKey(field.Name)) continue;
                TargetValues.Add(field.Name, new QA_Level_TargetValues());
                isDirty = true;
            }

            //Check if items are not set
            foreach (var item in TargetValues.Keys.Where(item => Math.Abs(TargetValues[item].RedlineValue - TargetValues[item].GreenLineValue) < .1f))
                Debug.LogError("Field - " + item + " is not setup properly. Some Values overlap!");

#if UNITY_EDITOR
            if (isDirty)
                UnityEditor.EditorUtility.SetDirty(this);
#endif
        }

        private static FieldInfo[] GetFields(Type i_Type)
        {
            List<FieldInfo> allFields = new List<FieldInfo>();

            const BindingFlags flags = BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic;

            allFields.AddRange(i_Type.GetFields(flags));

            return allFields.ToArray();
        }

        public QA_Level_TargetValues GetValues(string i_FieldName)
        {
            //TODO - not the prettiest solution. Maybe we should change it
            //When creating dummy graphs
            if (string.IsNullOrEmpty(i_FieldName))
                return new QA_Level_TargetValues();

            if (TargetValues.ContainsKey(i_FieldName))
                return TargetValues[i_FieldName];
            else
            {
                Debug.LogError("Couldn't find Field - " + i_FieldName + ". Returning default values");
                return new QA_Level_TargetValues();
            }
        }
    }
#endif
}