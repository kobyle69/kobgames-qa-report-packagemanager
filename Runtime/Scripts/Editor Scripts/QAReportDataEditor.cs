using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public struct QA_GraphDataGroup
    {
        public int CurrGraphID;
        public int NextGraphID     => GetIDNormalized(CurrGraphID + 1);
        public int PreviousGraphID => GetIDNormalized(CurrGraphID - 1);


        public List<QA_GraphData> Graphs;
        public QA_GraphData       CurrGraph     => Graphs[CurrGraphID];
        public QA_GraphData       NextGraph     => Graphs[GetIDNormalized(CurrGraphID + 1)];
        public QA_GraphData       PreviousGraph => Graphs[GetIDNormalized(CurrGraphID - 1)];


        //TODO - if ID is less than -Graph.count it won't work
        private int GetIDNormalized(int i_ID)
        {
            if (i_ID > 0)
                i_ID = i_ID % Graphs.Count;
            else if (i_ID < 0)
            {
                i_ID = Graphs.Count + i_ID;
            }

            return i_ID;
        }

        public QA_GraphDataGroup(int i_GraphID)
        {
            CurrGraphID = i_GraphID;
            Graphs      = new List<QA_GraphData>();
        }

        public void AddGraph(QA_GraphData i_GraphData)
        {
            Graphs.Add(i_GraphData);
        }

        public QA_GraphDataGroup SetGraphID(int i_GraphID)
        {
            CurrGraphID = i_GraphID;

            return this;
        }

        public static QA_GraphDataGroup GetDefault()
        {
            QA_GraphDataGroup Group = new QA_GraphDataGroup(0);
            Group.AddGraph(QA_GraphData.GetDefault());
            return Group;
        }
    }

    public struct QA_GraphData
    {
        /// <summary>
        /// Var name that created this graph
        /// </summary>
        public string FieldName;

        /// <summary>
        /// Name that appears on the group (based on FieldName)
        /// </summary>
        public string GraphSection;

        /// <summary>
        /// Type of graph - Average - Max - Min
        /// </summary>
        public string GraphType;

        public List<QA_Levels_Entries> Entries;

        public static QA_GraphData GetDefault()
        {
            return new QA_GraphData() { Entries = new List<QA_Levels_Entries>() };
        }

        public void Normalize()
        {
            if (Entries.Count == 1)
            {
                Entries.Add(Entries[0]);
            }
        }
    }


//TODO - naming between QA_Levels_Entries and QA_LevelDataEntries is misleading
    public struct QA_Levels_Entries
    {
        public int                     Level;
        public float                   Value;
        public List<QA_LevelDataEntry> DataEntries;

        public QA_Levels_Entries(int i_Level, float i_Value, List<QA_LevelDataEntry> i_DataEntries)
        {
            Level       = i_Level;
            Value       = i_Value;
            DataEntries = i_DataEntries;
        }
    }

    [System.Serializable]
    public struct QA_Level_TargetValues
    {
        public bool InvertGraph;

        [OnValueChanged(nameof(OnValueChanged))]
        public float RedlineValue;

        [OnValueChanged(nameof(OnValueChanged))]
        public float GreenLineValue;

        private void OnValueChanged()
        {
            if (QAReportWindow_Editor.Instance != null)
                QAReportWindow_Editor.Instance.UpdateGraphsBackgrounds();
        }
    }
#endif
}