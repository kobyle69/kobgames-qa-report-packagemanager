using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;
using System.Reflection;
using System.Linq;
using System;
using System.Globalization;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public class QAReportGroup_LevelDetails : QAReportGroup
    {
        private QAReportGroup_LevelDetailsEntry m_TitleEntry;
        private ListView                        m_ListViewElement;

        private Action<QA_LogMessages> OnLogSelected;

        public QAReportGroup_LevelDetails(string i_Title, VisualElement i_RootElement, string i_ParentElementToSearch, float i_FlexGrow = 0, Action<QA_LogMessages> i_OnLogSelected = null)
            : base(i_Title, i_RootElement, i_ParentElementToSearch, i_FlexGrow)
        {
            OnLogSelected = i_OnLogSelected;

            QA_LevelDataEntry entry = new QA_LevelDataEntry(eLevelDataEntry.Values, "VALUE", "TIME STAMP", "PLAYER POS", "LEVEL");
            m_TitleEntry = new QAReportGroup_LevelDetailsEntry(m_GroupEntriesElement, entry, true);

            m_ParentElement.style.flexGrow       = 1;
            m_GroupEntriesElement.style.flexGrow = 1;

        }

        public override void CreateFields(FieldInfo[] i_FieldInfo, object i_ClassData)
        {
        }

        public void SetData(string i_GroupTitle, QA_Levels_Entries i_LevelEntries)
        {
            Clear();

            m_ParentElement.style.display = DisplayStyle.Flex;

            m_TitleElement.text = i_GroupTitle;


            if (i_LevelEntries.DataEntries.Count == 0) return;

            VisualTreeAsset m_LevelDetailsVisualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(QAConstants.GroupEntryLevelDetailsUXMLPath);

            Func<VisualElement> makeItem = () => m_LevelDetailsVisualTree.Instantiate();

            Action<VisualElement, int> bindItem = (i_element, i_ID) =>
            {
                string valueEntry = i_LevelEntries.DataEntries[i_ID].Value;
                if (i_LevelEntries.DataEntries.FirstOrDefault().EntryType == eLevelDataEntry.Messages)
                {
                    QA_LogMessages Message = QA_LogMessages.FromJSon(valueEntry);
                    valueEntry = Message.Log;
                }

                string timestampEntry = i_LevelEntries.DataEntries[i_ID].TimeStamp == "" ? "" : Math.Round(float.Parse(i_LevelEntries.DataEntries[i_ID].TimeStamp, QAReportStaticValues.s_CultureInfo), 0).ToString();

                QA_LevelDataEntry entry = new QA_LevelDataEntry(i_LevelEntries.DataEntries[i_ID].EntryType, valueEntry, timestampEntry, i_LevelEntries.DataEntries[i_ID].PlayerPos, i_LevelEntries.DataEntries[i_ID].LevelID);
                //TODO - verify the impact on performance
                QAReportGroup_LevelDetailsEntry details = new QAReportGroup_LevelDetailsEntry(i_element, entry);

                m_TitleEntry.SetColumnsVisibility(details.ColumnsVisibility);
            };


            //TODO - itemHeight is returning 0 in some instances. this should be fixed instead of using 40 directly
            int itemHeight = Mathf.CeilToInt(m_TitleEntry.m_LevelDetailsElement.resolvedStyle.height);
            m_ListViewElement = new ListView(i_LevelEntries.DataEntries, 30, makeItem, bindItem);


            m_ListViewElement.Q<VisualElement>(QAConstants.ElementListViewContainer).RegisterCallback<GeometryChangedEvent>((evt) =>
            {
                m_ListViewElement.style.flexGrow                     = 1.0f;
                m_TitleEntry.m_LevelDetailsElement.style.marginRight = m_ListViewElement.Q<VisualElement>(QAConstants.ElementListViewViewport).resolvedStyle.marginRight;
            });

            m_ListViewElement.onSelectionChange += (obj) =>
            {
                if (i_LevelEntries.DataEntries.FirstOrDefault().EntryType == eLevelDataEntry.Messages)
                {
                    QA_LogMessages Message = QA_LogMessages.FromJSon(((QA_LevelDataEntry)obj.First()).Value);
                    OnLogSelected?.Invoke(Message);
                }
            };

            m_GroupEntriesElement.Add(m_ListViewElement);

        }

        //TODO - should it be public?
        public void Clear()
        {
            m_TitleElement.text = "LEVEL ";

            //TODO - this should be cached from the start and only update when needed
            if (m_ListViewElement != null && m_GroupEntriesElement.Contains(m_ListViewElement))
                m_GroupEntriesElement.Remove(m_ListViewElement);
        }
    }
#endif
}