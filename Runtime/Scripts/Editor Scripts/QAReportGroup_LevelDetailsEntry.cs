using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;
using System.Reflection;
using System.Linq;
using System;

namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR
    public struct LevelDetailsEntryVisibility
    {
        public bool Value;
        public bool TimeStamp;
        public bool PlayerPos;
        public bool LevelID;
    }

    public class QAReportGroup_LevelDetailsEntry
    {
        protected VisualTreeAsset m_LevelDetailsVisualTree;

        //TODO - this shouldn't be public
        public VisualElement m_LevelDetailsElement;

        //TODO - not a fan of manually setting this values in every place
        private Label m_LabelValue;
        private Label m_LabelTimeStamp;
        private Label m_LabelPlayerPos;
        private Label m_LabelLevelID;

        private VisualElement m_VisualElementValue;
        private VisualElement m_VisualElementTimeStamp;
        private VisualElement m_VisualElementPlayerPos;
        private VisualElement m_VisualElementLevelID;


        private LevelDetailsEntryVisibility m_ColumnsVisibility;
        public  LevelDetailsEntryVisibility ColumnsVisibility => m_ColumnsVisibility;

        public QAReportGroup_LevelDetailsEntry(VisualElement i_ParentElement, QA_LevelDataEntry i_Entry, bool i_IsTitle)
        {
            m_LevelDetailsVisualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(QAConstants.GroupEntryLevelDetailsUXMLPath);

            m_LevelDetailsElement = m_LevelDetailsVisualTree.Instantiate();

            SetElements();

            i_ParentElement.Add(m_LevelDetailsElement);

            Show(i_Entry, i_IsTitle);
        }

        public QAReportGroup_LevelDetailsEntry(VisualElement i_LevelDetailsElement, QA_LevelDataEntry i_Entry)
        {
            m_LevelDetailsElement = i_LevelDetailsElement;

            SetElements();

            Show(i_Entry);
        }

        private void SetElements()
        {
            m_LabelValue     = m_LevelDetailsElement.Q<Label>(QAConstants.ElementEntryLevelDetailsValue);
            m_LabelTimeStamp = m_LevelDetailsElement.Q<Label>(QAConstants.ElementEntryLevelDetailsTimeStamp);
            m_LabelPlayerPos = m_LevelDetailsElement.Q<Label>(QAConstants.ElementEntryLevelDetailsPlayerPos);
            m_LabelLevelID   = m_LevelDetailsElement.Q<Label>(QAConstants.ElementEntryLevelDetailsLevelID);


            m_VisualElementValue     = m_LabelValue.parent;
            m_VisualElementTimeStamp = m_LabelTimeStamp.parent;
            m_VisualElementPlayerPos = m_LabelPlayerPos.parent;
            m_VisualElementLevelID   = m_LabelLevelID.parent;
        }

        public void Show(QA_LevelDataEntry i_Entry, bool i_IsTitle = false)
        {
            m_LevelDetailsElement.style.display = DisplayStyle.Flex;
            m_LevelDetailsElement.BringToFront();

            //TODO - we might have cases where we have SingleEntry which is not Time. We need to be careful in such cases
            if(i_Entry.EntryType == eLevelDataEntry.SingleEntry)
                m_LabelValue.text = i_Entry.Value.ToTime();
            else
                m_LabelValue.text     = i_Entry.Value.ShortUpToMillion();
            
            m_LabelTimeStamp.text = i_Entry.TimeStamp.ToTime();
            m_LabelPlayerPos.text = i_Entry.PlayerPos;
            m_LabelLevelID.text   = i_Entry.LevelID;


            if (i_IsTitle)
            {
                m_LabelValue.style.unityFontStyleAndWeight =
                    m_LabelTimeStamp.style.unityFontStyleAndWeight =
                        m_LabelPlayerPos.style.unityFontStyleAndWeight =
                            m_LabelLevelID.style.unityFontStyleAndWeight =
                                FontStyle.Bold;

                m_LabelValue.style.backgroundColor =
                    m_LabelTimeStamp.style.backgroundColor =
                        m_LabelPlayerPos.style.backgroundColor =
                            m_LabelLevelID.style.backgroundColor =
                                new StyleColor(QAConstants.ElementEntryLevelDetailsTitleBackgroundColor);
            }

            SetColumnsVisibility();
        }

        public void Hide()
        {
            m_LevelDetailsElement.style.display = DisplayStyle.None;
        }

        public void SetColumnsVisibility()
        {
            SetColumnsVisibility(m_LabelValue.text != "", m_LabelTimeStamp.text != "", m_LabelPlayerPos.text != "", m_LabelLevelID.text != "");
        }

        public void SetColumnsVisibility(LevelDetailsEntryVisibility i_ColumnsVisibility)
        {
            SetColumnsVisibility(i_ColumnsVisibility.Value, i_ColumnsVisibility.TimeStamp, i_ColumnsVisibility.PlayerPos, i_ColumnsVisibility.LevelID);
        }

        public void SetColumnsVisibility(bool i_ColumnValue, bool i_ColumnTimeStamp, bool i_ColumnPlayerPos, bool i_ColumnLevelID)
        {
            m_VisualElementValue.style.display     = i_ColumnValue ? DisplayStyle.Flex : DisplayStyle.None;
            m_VisualElementTimeStamp.style.display = i_ColumnTimeStamp ? DisplayStyle.Flex : DisplayStyle.None;
            m_VisualElementPlayerPos.style.display = i_ColumnPlayerPos ? DisplayStyle.Flex : DisplayStyle.None;
            m_VisualElementLevelID.style.display   = i_ColumnLevelID ? DisplayStyle.Flex : DisplayStyle.None;


            m_ColumnsVisibility.Value     = i_ColumnValue;
            m_ColumnsVisibility.TimeStamp = i_ColumnTimeStamp;
            m_ColumnsVisibility.PlayerPos = i_ColumnPlayerPos;
            m_ColumnsVisibility.LevelID   = i_ColumnLevelID;

        }
    }
#endif
}