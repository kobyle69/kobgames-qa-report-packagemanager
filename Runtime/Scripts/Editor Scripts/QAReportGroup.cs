using System.Reflection;
using UnityEngine.UIElements;
using UnityEditor;


namespace KobGamesSDKSlim.QAReport.UI
{
#if UNITY_EDITOR

    public abstract class QAReportGroup
    {
        protected VisualElement m_ParentElement;
        protected Label         m_TitleElement;
        protected VisualElement m_GroupEntriesElement;

        protected VisualTreeAsset m_EntryVisualTree;

        private bool m_IsOpened = true;

        public QAReportGroup(string i_Title, VisualElement i_RootElement, string i_ParentElementToSearch, float i_FlexGrow = 0)
        {
            m_ParentElement                = i_RootElement.Q<VisualElement>(i_ParentElementToSearch);
            m_ParentElement.style.flexGrow = i_FlexGrow;

            m_GroupEntriesElement = m_ParentElement.Q<VisualElement>(QAConstants.ElementGroupEntries);
            m_TitleElement        = m_ParentElement.Q<Label>(QAConstants.ElementGroupTitle);

            m_EntryVisualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(QAConstants.GroupEntryUXMLPath);

            m_TitleElement.text = i_Title;

            //TODO - this should be set per group
            m_TitleElement.RegisterCallback<MouseDownEvent>((evt) =>
            {
                m_IsOpened                          = !m_IsOpened;
                m_GroupEntriesElement.style.display = m_IsOpened ? DisplayStyle.Flex : DisplayStyle.None;
            });
        }

        public abstract void CreateFields(FieldInfo[] i_FieldInfo, object i_ClassData);

        public void SetTitle(string i_Title)
        {
            m_TitleElement.text = i_Title;
        }
    }
#endif
}