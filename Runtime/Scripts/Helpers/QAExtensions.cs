using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

namespace KobGamesSDKSlim.QAReport
{
    public static class QAExtensions
    {
        public static string SpaceWhenCapitalOrSymbol(this string i_string)
        {
            string s = "";
            for (int j = 0; j < i_string.Length; j++)
            {
                if (i_string[j] == '_')
                {
                    s += " ";
                    continue;
                }

                if (j > 0 && char.IsUpper(i_string[j]) && i_string[j - 1] != '_')
                {
                    if (!char.IsUpper(i_string[j - 1]))
                    {
                        s += " ";
                    }
                }

                s += i_string[j];
            }

            return s;
        }

        public static string ShortUpToMillion(this float i_float, int i_DecimalPlaces = 1)
        {
            if (i_float < 1000)
                return Math.Round(i_float, i_DecimalPlaces).ToString();
            else if (i_float < 1000000)
            {
                return Math.Round(i_float / 1000, i_DecimalPlaces) + "K";
            }
            else
                return Math.Round(i_float / 1000000, i_DecimalPlaces) + "M";
        }

        public static string ShortUpToMillion(this string i_string, int i_DecimalPlaces = 1)
        {
            float parsedFloat;

            if (float.TryParse(i_string, out parsedFloat))
            {
                return parsedFloat.ShortUpToMillion(i_DecimalPlaces);
            }

            return i_string;
        }

        public static string ToTime(this string i_Seconds, bool i_Round = false)
        {
            float parsedValue;
            if(float.TryParse(i_Seconds, NumberStyles.Any, QAReportStaticValues.s_CultureInfo, out parsedValue))
                return ToTime(parsedValue);
            
            return i_Seconds;
        }

        public static string ToTime(this float i_Seconds, bool i_Round = false)
        {
            i_Seconds *= 1000;
            TimeSpan t = TimeSpan.FromMilliseconds(i_Seconds);

            string finalString = "";
            
            if (t.Hours > 0)
            {
                finalString = string.Format("{0:D2}h:{1:D2}m", t.Hours, t.Minutes);
                return finalString;
            }

            if (t.Minutes > 0)
            {
                
                finalString = string.Format("{0:D2}m:{1:D2}s", t.Minutes, t.Seconds);
                
                return finalString;
            }

            if (t.Milliseconds > 0)
            {
                if(i_Round)
                    finalString = string.Format("{0}.{1}s", t.Seconds, (int)(t.Milliseconds * .01f));
                else
                {
                    finalString = string.Format("{0}.{1}s", t.Seconds, t.Milliseconds);
                }
            }
            else 
                finalString = string.Format("{0}s", t.Seconds);
                
            return finalString;
        }
    }
}