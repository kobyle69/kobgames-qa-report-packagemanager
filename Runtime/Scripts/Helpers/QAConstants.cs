using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace KobGamesSDKSlim.QAReport
{
    public class QAConstants
    {
	    
#if PACKAGE_EDITOR
        public static string PluginParentPath = Application.dataPath;
	    public static string PackageParentFolder = "Assets";

#else
	    public static string PackageParentFolder = "Packages";
        public static string PluginParentPath = new DirectoryInfo(Application.dataPath).Parent + "/" + PackageParentFolder;

#endif
	    public static string PackageFolder = "/com.kobgames.qa_report";

	    public static string PluginPath = PluginParentPath + PackageFolder;

	    
#if UNITY_EDITOR
	    public static string ParentPath => PluginParentPath + PackageFolder;
#else
    public static string ParentPath => Application.persistentDataPath + PackageFolder;
#endif
        public static string JSONFolderPath => ParentPath + "/JSON/";

        //USS
        public static string USSPath                      = PackageParentFolder + PackageFolder + "/Runtime/UIElements/USS/";
        public static string MainStyleSheetPath           = USSPath + "QAReportEditorWindow.uss";
        public static string SelectorButtonStyleSheetPath = USSPath + "QAReportEditorSelectorButton.uss";

        //UXML
        public static string UXMLPath                  = PackageParentFolder + PackageFolder + "/Runtime/UIElements/UXML/";
        public static string WindowEditorXMLPath       = UXMLPath + "QAReport_WindowEditor.uxml";
        public static string WindowMainXMLPath         = UXMLPath + "QAReport_WindowMain.uxml";
        public static string WindowLevelDetailsXMLPath = UXMLPath + "QAReport_WindowLevelDetails.uxml";

        public static string GroupEntryUXMLPath             = UXMLPath + "QAReport_GroupLabelEntry.uxml";
        public static string GroupEntryLevelUXMLPath        = UXMLPath + "QAReport_GroupLevelEntry.uxml";
        public static string GroupEntryLevelDetailsUXMLPath = UXMLPath + "QAReport_GroupLevelDetailsEntry.uxml";

        public static string LevelDetailsGraphUXMLPath = UXMLPath + "QAReport_LevelDetailsGraph.uxml";


        //VISUAL ELEMENTS
		public static string ElementWindowMain   = "WindowMain";
		public static string ElementLevelDetails = "LevelDetails";

		
        public static string ElementLoadButton = "LoadButton";
        public static string ElementQAVersion  = "QAVersion";
        public static string ElementLoadedJSON = "LoadedJSON";


        public static string ElementBuildInfoGroup         = "BuildInfo";
        public static string ElementDeviceInfoGroup        = "DeviceInfo";
        public static string ElementGlobalInfoGroup        = "GlobalInfo";
        public static string ElementLevelsInfoGroup        = "LevelsInfo";
        public static string ElementLevelDetailsGraphGroup = "LevelDetailsGraphGroup";
        public static string ElementLevelDetailsGroup      = "LevelDetailsGroup";


        public static string ElementGroupTitle    = "GroupTitle";
        public static string ElementGroupEntries  = "GroupEntries";
        public static string ElementEntryTitle    = "EntryTitle";
        public static string ElementEntryValue    = "EntryValue";
        public static string ElementEntryRowLevel = "EntryRowLevel";

        public static string ElementBackButton = "BackButton";
        public static string ElementPrevButton = "PrevGraphButton";
        public static string ElementNextButton = "NextGraphButton";
        public static string ElementMainSpacer = "MainSpacer";


        public static string ElementEntryLevelMinMaxValues     = "EntryMinMaxValues";
        public static string ElementEntryLevelAverageValue     = "EntryLevelAverageValue";
        public static string ElementEntryLevelMinValue         = "EntryLevelMinValue";
        public static string ElementEntryLevelMaxValue         = "EntryLevelMaxValue";
        public static string ElementEntryLevelBackground       = "EntryLevelBackground";
        public static string ElementEntryLevelBackgroundGreen  = "EntryLevelBackgroundGreen";
        public static string ElementEntryLevelBackgroundOrange = "EntryLevelBackgroundOrange";
        public static string ElementEntryLevelBackgroundRed    = "EntryLevelBackgroundRed";
        public static string ElementEntryLevelGraphTypeLabel   = "EntryLevelGraphTypeLabel";

        public static string ElementEntryLevelDottedLine  = "EntryLevelDottedLine";
        public static string ElementDottedLineLevelTop    = "DottedLineLevelTop";
        public static string ElementDottedLineValueTop    = "DottedLineValueTop";
        public static string ElementDottedLineLevelBottom = "DottedLineLevelBottom";
        public static string ElementDottedLineValueBottom = "DottedLineValueBottom";

        public static string ElementEntryLevelDetailsValue                = "EntryLevelDetailsValue";
        public static string ElementEntryLevelDetailsTimeStamp            = "EntryLevelDetailsTimestamp";
        public static string ElementEntryLevelDetailsPlayerPos            = "EntryLevelDetailsPlayerPos";
        public static string ElementEntryLevelDetailsLevelID              = "EntryLevelDetailsLevelID";
        public static Color  ElementEntryLevelDetailsTitleBackgroundColor = new Color(0, 0, 0, .25f);
        public static string ElementLogMessageDetailsParent               = "LogMessageDetails";
        public static string ElementLogMessageLogValue                    = "LogValue";
        public static string ElementLogMessageStacktraceValue             = "StackTraceValue";
        public static string ElementLogCopy                               = "LogCopy";



        public static string ElementListViewViewport  = "unity-content-viewport";
        public static string ElementListViewContainer = "unity-content-container";




        //SECTIONS
        public static string SectionBuildInfo  = "BUILD INFO";
        public static string SectionDeviceInfo = "DEVICE INFO";
        public static string SectionGlobalInfo = "GLOBAL INFO";
        public static string SectionLevelsInfo = "LEVELS INFO";

    }
}