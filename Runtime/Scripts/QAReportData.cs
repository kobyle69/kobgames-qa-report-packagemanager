using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;
using System.Linq;

namespace KobGamesSDKSlim.QAReport
{
    [System.Serializable]
    public struct QA_LogMessages
    {
        public string Log;
        public string StackTrace;

        public QA_LogMessages(string i_Log, string i_StackTrace)
        {
            Log        = i_Log;
            StackTrace = i_StackTrace;
        }

        public string ToJSon()
        {
            return JsonUtility.ToJson(this);
        }

        public static QA_LogMessages FromJSon(string i_Message)
        {
            return (QA_LogMessages)JsonUtility.FromJson(i_Message, typeof(QA_LogMessages));
        }
    }


    [Serializable]
    public struct QA_BuildInfo
    {
        public string ReportVersion;
        public string Name;
        public string BundleID;
        public string Version;

        public string UnityVersion;
        //NOT SUPPORTED
        //public string BuildNumber;

        public void Set(string i_BuildName, string i_BundleID, string i_BuildVersion, string i_UnityVersion, string i_ReportVersion)
        {
            Name          = i_BuildName;
            BundleID      = i_BundleID;
            Version       = i_BuildVersion;
            UnityVersion  = i_UnityVersion;
            ReportVersion = i_ReportVersion;
        }
    }

    [Serializable]
    public struct QA_DeviceInfo
    {
        public string OS;
        public string CPU;
        public string SystemMemory;
        public string GPU;
        public string GPU_Memory;
        public string GraphicsAPI;
        public string Resolution;
        public string ScreenSize;


        public void Set(string i_DeviceOS, string i_DeviceCPU, string i_DeviceSystemMemory, string i_DeviceGPU, string i_DeviceGPUMemory, string i_DeviceGPUType, string i_DeviceResolution, string i_DeviceScreenSize)
        {
            OS           = i_DeviceOS;
            CPU          = i_DeviceCPU;
            SystemMemory = i_DeviceSystemMemory;
            GPU          = i_DeviceGPU;
            GPU_Memory   = i_DeviceGPUMemory;
            GraphicsAPI  = i_DeviceGPUType;
            Resolution   = i_DeviceResolution;
            ScreenSize   = i_DeviceScreenSize;
        }
    }

    [Serializable]
    public class QA_GlobalInfo
    {
        public float LoadingTime;
        public float LoadingMemory;
        public float LoadingGC;

        //Debugs outside of game loop
        public QA_LevelDataEntries Warnings = new QA_LevelDataEntries();
        public QA_LevelDataEntries Errors   = new QA_LevelDataEntries();


        public void Set(float i_LoadingTime, float i_LoadingMemory, float i_LoadingGC)
        {
            LoadingTime   = i_LoadingTime;
            LoadingMemory = i_LoadingMemory;
            LoadingGC     = i_LoadingGC;
        }
    }

    public enum eLevelDataEntry
    {
        SingleEntry,
        Messages,
        Values
    }

    [Serializable]
    public class QA_LevelDataEntries
    {
        public QA_LevelDataEntry       FirstEntry => Entries.FirstOrDefault();
        public List<QA_LevelDataEntry> Entries = new List<QA_LevelDataEntry>();

        public void AddEntry(QA_LevelDataEntry i_Entry)
        {
            Entries.Add(i_Entry);
        }

        public void SetUniqueEntry(QA_LevelDataEntry i_Entry)
        {
            Entries.Clear();
            AddEntry(i_Entry);
        }

        //TODO - check if it is working properly
        public void Merge(QA_LevelDataEntries i_Entries)
        {
            Entries.AddRange(i_Entries.Entries);
        }
    }

//TODO - we need a more robust solution since we might want to add arbitrary info
    [Serializable]
    public struct QA_LevelDataEntry
    {
        public eLevelDataEntry EntryType;
        public string          Value;
        public string          TimeStamp;
        public string          PlayerPos;
        public string          LevelID;


        public QA_LevelDataEntry(eLevelDataEntry i_EntryType, string i_Value, string i_TimeStamp = "", string i_PlayerPos = "", string i_LevelID = "")
        {
            EntryType = i_EntryType;
            Value     = i_Value;
            TimeStamp = i_TimeStamp;
            PlayerPos = i_PlayerPos;
            LevelID   = i_LevelID;
        }
    }

    [Serializable]
    public class QA_LevelInfo
    {
        public QA_LevelDataEntries TimePlaying = new QA_LevelDataEntries();

        public QA_LevelDataEntries Warnings = new QA_LevelDataEntries();
        public QA_LevelDataEntries Errors   = new QA_LevelDataEntries();

        public QA_LevelDataEntries FPS          = new QA_LevelDataEntries();
        public QA_LevelDataEntries Batches      = new QA_LevelDataEntries();
        public QA_LevelDataEntries SetPassCalls = new QA_LevelDataEntries();

        //public QA_LevelDataEntries  Vertices;
        public QA_LevelDataEntries Triangles = new QA_LevelDataEntries();

        public QA_LevelDataEntries SystemMemory = new QA_LevelDataEntries();
        public QA_LevelDataEntries GC           = new QA_LevelDataEntries();

        public void Merge(QA_LevelInfo i_LevelInfo)
        {
            if (float.Parse(i_LevelInfo.TimePlaying.FirstEntry.Value, QAReportStaticValues.s_CultureInfo) > float.Parse(TimePlaying.FirstEntry.Value, QAReportStaticValues.s_CultureInfo))
                TimePlaying.SetUniqueEntry(i_LevelInfo.TimePlaying.FirstEntry);

            Warnings.Merge(i_LevelInfo.Warnings);
            Errors.Merge(i_LevelInfo.Warnings);

            FPS.Merge(i_LevelInfo.FPS);
            Batches.Merge(i_LevelInfo.Batches);
            SetPassCalls.Merge(i_LevelInfo.SetPassCalls);

            //Vertices.AddRange(i_LevelInfo.Vertices);
            Triangles.Merge(i_LevelInfo.Triangles);

            SystemMemory.Merge(i_LevelInfo.SystemMemory);
            GC.Merge(i_LevelInfo.GC);
        }
    }

    [System.Serializable]
    public class DictionaryLevelNumberLevelInfo : UnitySerializedDictionary<int, QA_LevelInfo>
    {
    }

    [Serializable]
    public class QAReportData
    {
        public QA_BuildInfo  BuildInfo;
        public QA_DeviceInfo DeviceInfo;

        public QA_GlobalInfo                  GlobalInfo = new QA_GlobalInfo();
        public DictionaryLevelNumberLevelInfo LevelInfo = new DictionaryLevelNumberLevelInfo();


        //TODO
        //Analytics correctly reported
        //Amount of textures
        //Textures size
    }
}