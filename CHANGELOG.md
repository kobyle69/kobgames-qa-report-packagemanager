### v0.7.1
- Fixed compilation issue in unity 2021.3.15 due to nonexistent UnityEditor.UIElements
- Fixed consecutive loads throwing an error
### v0.7.0
#### Compatibility
- Compatibility with SDK version release at 08/08/2022 (Project Validator changes)
### v0.6.0
#### Bug Fixes
- Fixed main ScrollView showing arrows even when that window is closed
#### Improvement
- Restructure of QAReport.cs (mainly)
- Font folder is not part of Samples anymore
- Better integration with Project Validator - now we can fetch Validator Logs from QAReport Validator

### v0.5.0
#### Bug Fixes
- trying to fix bug with fps values in some devices
- fix canvas render order
#### Improvement
- UI updated to best fit smaller resolutions
- triangles target values updated

### v0.4.0
- Complying with new Validate Project pipeline (URP Overlay Canvas issues)

### v0.3.1
- Removing style reference from UXML
- Including specific font so Project Verification is happy (font issues)

### v0.3
#### Organization
- Moved from UnityPackage to Unity Package Manager
#### Improvement
- We no longer need to reference TargetValues

### v0.2
#### Organization
- Folder name changed (you should overwrite everything if updating)
- Namespace creation
#### Improvement
- Saving Report version for easy comparison
- Support for json files
- Timestamp will show seconds/minutes

### v0.1.2
#### Bug Fixes
- Parsing error would show when losing 2 times in a row.

### v0.1.1
#### Bug Fixes
- Problems with localization when saving data on different devices
- Annoying auto save on Application Quit removed
- Folder changed from Assets to 3rd Party

### v0.1
Base functionality. Ability to track:
- Build Info
    - Name
    - Bundle ID
    - Version
    - Unity Version

- Device Info
    - OS
    - CPU and GPU
    - Graphics API
    - Memory *(CPU and GPU)*
    - Resolution
    - Screen Size

- Global Info
    - Loading Time
    - Loading Memory and GC
    - Warnings and Errors

- Levels Info
    - Time Played
    - Warnings and Errors
    - FPS
    - DrawCalls
    - Triangles
    - Memory and GC